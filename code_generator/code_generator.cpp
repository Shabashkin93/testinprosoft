/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */
#include <iostream>
using namespace std;

class CodeGenerator {
    private:
        virtual std::string* someCodeRelatedThing()=0;
    public:
        CodeGenerator()
        {}
        void generateCode(){
            cout << (char*)someCodeRelatedThing() << endl;
        }
};

class java : public CodeGenerator{
    private:
        std::string* someCodeRelatedThing(){
            return (std::string*)"generated on java\n";
        }
};

class c_plus_plus: public CodeGenerator{
    private:
        std::string* someCodeRelatedThing(){
            return (std::string*)"generated on C++\n";
        }
};

class php: public CodeGenerator{
    private:
        std::string* someCodeRelatedThing(){
            return (std::string*)"generated on PHP\n";
        }
};

int main(int argc, char *argv[])
{
    java a;
    c_plus_plus b;
    php c;
    a.generateCode();
    b.generateCode();
    c.generateCode();
    return 0;
}
