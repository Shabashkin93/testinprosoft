/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */
#include "linear_search.h"

typedef struct testStruct {
    int a;
    int b;
}testStruct ;

int main(void){
    int a = 3;
    int b = 8;
    int c[]={1,2,3};
    printf("index int\t%d\n", genericLinearSearch(c, &a, 3, sizeof(int)));
    printf("index int\t%d\n", genericLinearSearch(c, &b, 3, sizeof(int)));
    float a1 = 3.23;
    float c1[]={1.66, 3.23, 3.236, 4.16};
    printf("index float\t%d\n", genericLinearSearch(c1, &a1, 4, sizeof(float)));
    char a2 = 'R';
    char c2[]={'t','f','F','g','R','k','O'};
    printf("index char\t%d\n", genericLinearSearch(c2, &a2, 7, sizeof(char)));
    testStruct a3 = {1,3};
    testStruct c3[]={{2,3},{1,3},{8,9}};
    printf("index struct\t%d\n", genericLinearSearch(c3, &a3, 3, sizeof(testStruct)));

    return 0;
}
