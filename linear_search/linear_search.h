/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */
#ifndef LINEAR_SEARCH
#define LINEAR_SEARCH
#include <stdio.h>
#include <string.h>

int genericLinearSearch(void *array, void *key, int len, int sizeOneElement);

#endif
