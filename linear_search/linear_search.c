/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */
#include "linear_search.h"

int genericLinearSearch(void *array, void *key, int len, int sizeOneElement){
    for (int i=0; i<len; i++){
        void *elementAdress = (char*)array + i*sizeOneElement;
        if (memcmp(key,elementAdress,sizeOneElement)==0){
            return i;
        }
    }
    return -1;
}
