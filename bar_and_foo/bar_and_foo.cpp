/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */
#include <iostream>
using namespace std;

class Foo
{
public:
    Foo(){
      cout << "конструктор по умолчанию Foo\n";
    }
    Foo(int j) { i=new int[j]; }
    virtual ~Foo() { cout << "деструктор Foo\n";delete i; }
private:
    int* i;
};

class Bar: public Foo
{
public:
    Bar(int j) { i=new char[j]; }
    ~Bar() { cout << "деструктор Bar\n"; delete i; }
private:
    char* i;
};


int main(int argc, char *argv[])
{
    Foo* f=new Foo(100);
    Foo* b=new Bar(200);
    *f=*b;
    delete f;
    delete b;
    cout << "end\n";
    return 0;
}
