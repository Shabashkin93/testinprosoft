/* Copyright ProsoftSystems Test task. All right not reserved
 * Author: Shabashkin.o
 * 2018
 * */

/*Что не так в этом коде? Перечислите, какие недостатки вы видите.
 * Предложите свой вариант рефакторинга.*/

#include <stdio.h>
#include <iostream>

class Feature
{
public:
    enum FeatureType {eUnknown, eCircle, eTriangle, eSquare};

    Feature() : type(eUnknown), points(0) {    }

    ~Feature()
    {
        if (points)
            delete [] points;// нужны скобки тк удаляем массив
    }

    bool isValid()
    {
        return type != eUnknown;
    }

    bool read(FILE* file)
    {
        if (fread(&type, sizeof(FeatureType), 1, file) != sizeof(FeatureType))
            return false;
        short n = 0;
        bool res;
        switch (type)
        {
        case eCircle: n = 3; break;
        case eTriangle:    n = 6; break;
        case eSquare: n = 8; break;
        default: type = eUnknown; return false;
        }
        points = new double[n];
        if (!points)
            return false;
        res=fread(&points, sizeof(double), n, file) == n*sizeof(double);
        delete [] points;// во избежании утечек памяти надо удалить всё что выделели
        return res;
    }

    bool draw()
    {
        switch (type)
        {
        case eCircle: return drawCircle(points[0], points[1], points[2]);
        case eTriangle: return drawPoligon(points, 6);
        case eSquare: return drawPoligon(points, 8);
        /*"eUnknown" было в перечислении,
         *  но не было обозначено действия при передаче в switch "eUnknown" в качестве ключа
         *  так же не было default*/
        default: type = eUnknown; return false;
        }
    }

protected:
    bool drawCircle(double centerX, double centerY, double radius);// пусть возвращает либо True , либо Falshe в зависимости от успешности выполнения
    bool drawPoligon(double* points, int size);                    // пусть возвращает либо True , либо Falshe в зависимости от успешности выполнения
    FeatureType type;//порядок объявления должен совпадать с порядком инициализации
    double* points;
};

int main(int argc, char* argv[])
{
    Feature feature;
    try{
        FILE* file = fopen("features.dat", "r");                   //если файла нет или не можем его открыть то программа падает
        if (file != NULL){                                         //добавим проверку успешности открытия файла
            feature.read(file);
            fclose(file);                                          //после работы с файлом его надо закрыть
        }
        else { throw false; }
    }
    catch(...) {std::cout << "file not found or not read\n";}      //для наглядности при тестировании выведем сообщение об ошибке
    if (!feature.isValid())
        return 1;
    return 0;
}
