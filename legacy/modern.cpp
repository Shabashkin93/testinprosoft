#define LEGACY_SIZE 3
extern int *values; // должно быть extern int *values[];

class MyBlah {...};

class Adapter
{
public:
    Adapter()
    {
for (int i = 0; i < LEGACY_SIZE; ++i)
    		map_[values[i]] = new MyBlah (values[i]);
    }
private:
    std::map<int, MyBlah *> map_;
};

/*Нет деструктора
 *
